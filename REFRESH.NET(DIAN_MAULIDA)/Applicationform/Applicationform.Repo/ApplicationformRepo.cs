﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Applicantform.Models;

namespace Applicationform.Repo
{
    public class ApplicationformRepo
    {
        public static List<tbl_applicant> GetAllData()
        {
            List<tbl_applicant> dataAll = new List<tbl_applicant>();
            using (applicant_dbEntities db = new applicant_dbEntities())
            {
                dataAll = db.tbl_applicant.Where(a => a.Is_delete == false).ToList();

            }
            return dataAll;
        }
        public static string CreateData(tbl_applicant dataapplicant)
        {
            try
            {

                tbl_applicant applicant = new tbl_applicant();

                using (applicant_dbEntities db = new applicant_dbEntities())
                {

                    applicant.Created_by = "Dian";
                    applicant.Created_date = System.DateTime.Now;

                    applicant.Name = dataapplicant.Name;
                    applicant.Mobile_Phone_Number = dataapplicant.Mobile_Phone_Number;
                    applicant.Alternative_Phone_Number = dataapplicant.Alternative_Phone_Number;
                    applicant.Email = dataapplicant.Email;
                    applicant.Place_of_Birth = dataapplicant.Place_of_Birth;
                    applicant.Date_of_Birth = dataapplicant.Date_of_Birth;
                    applicant.Last_Education = dataapplicant.Last_Education;
                    applicant.Collage_University = dataapplicant.Collage_University;
                    applicant.Major = dataapplicant.Major;
                    applicant.Position_Apply = dataapplicant.Position_Apply;
                    applicant.Source = dataapplicant.Source;
                    applicant.Is_delete = false;
                    db.tbl_applicant.Add(applicant);
                    db.SaveChanges();
                }
                return "Berhasil";
            }
            catch (Exception e)
            {

                return e.Message.ToString();
            }
        }
        public static tbl_applicant GetDataById(int Id)
        {
            tbl_applicant data = new tbl_applicant();
            using (applicant_dbEntities db = new applicant_dbEntities())
            {
                data = db.tbl_applicant.Where(a => a.Id == Id).FirstOrDefault();
            }
            return data;
        }
        public static string UpdateData(tbl_applicant dataapplicant)
        {
            try
            {
                tbl_applicant applicant = new tbl_applicant();
                using (applicant_dbEntities db = new applicant_dbEntities())
                {

                    applicant = db.tbl_applicant.Where(a => a.Id == dataapplicant.Id).FirstOrDefault();


                    applicant.Updated_by = "Dian";
                    applicant.Updated_date = System.DateTime.Now;

                    applicant.Name = dataapplicant.Name;
                    applicant.Mobile_Phone_Number = dataapplicant.Mobile_Phone_Number;
                    applicant.Alternative_Phone_Number = dataapplicant.Alternative_Phone_Number;
                    applicant.Email = dataapplicant.Email;
                    applicant.Place_of_Birth = dataapplicant.Place_of_Birth;
                    applicant.Date_of_Birth = dataapplicant.Date_of_Birth;
                    applicant.Last_Education = dataapplicant.Last_Education;
                    applicant.Collage_University = dataapplicant.Collage_University;
                    applicant.Major = dataapplicant.Major;
                    applicant.Position_Apply = dataapplicant.Position_Apply;
                    applicant.Source = dataapplicant.Source;
                    applicant.Is_delete = false;


                    db.Entry(applicant).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return "Berhasil";
            }
            catch (Exception e)
            {

                return e.Message.ToString();
            }
        }
        public static string DeleteData(int Id)
        {
            try
            {
                tbl_applicant applicant = new tbl_applicant();
                using (applicant_dbEntities db = new applicant_dbEntities())
                {
                    applicant = db.tbl_applicant.Where(a => a.Id == Id).FirstOrDefault();
                    applicant.Is_delete = true;

                    applicant.Delete_by = "Dian";
                    applicant.Delete_date = System.DateTime.Now;

                    db.Entry(applicant).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return "Berhasil";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
                throw;
            }
        }
    }

}

