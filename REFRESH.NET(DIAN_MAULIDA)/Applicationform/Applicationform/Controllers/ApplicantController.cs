﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Applicantform.Models;
using Applicationform.Repo;
using Applicationform.ViewModels;

namespace Applicationform.Controllers
{
    public class ApplicantController : Controller
    {
        // GET: Applicant
        public ActionResult Index()
        {
            List<tbl_applicant> ListApplicant = ApplicationformRepo.GetAllData();
            return View(ListApplicant);
          
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Create(tbl_applicant dataApplicant)
        {

            string result = ApplicationformRepo.CreateData(dataApplicant);
            return Json(new { respon = result }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            
            tbl_applicant  dataApplicant = ApplicationformRepo.GetDataById(Id);
            return View(dataApplicant );
        }
        [HttpPost]
        public ActionResult Edit(tbl_applicant dataApplicant)
        {

            string result = ApplicationformRepo.UpdateData(dataApplicant );
            return Json(new { respon = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDataById(int Id)
        {
            tbl_applicant  data = new tbl_applicant ();
            using (applicant_dbEntities  db = new applicant_dbEntities ())
            {
                data = db.tbl_applicant.Where(a => a.Id == Id).FirstOrDefault();
            }

            return Json(new { respon = data }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Delete(int Id)
        {
            string result = ApplicationformRepo.DeleteData(Id);
            return Json(new { respon = result }, JsonRequestBehavior.AllowGet);
        }
    }
}